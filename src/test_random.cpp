//Move It! header fi
#include <moveit/move_group_interface/move_group.h>

int main(int argc, char **argv){
    ros::init(argc, argv, "test_random_node", ros::init_options::AnonymousName);

    //start a ROS spinning thread
    ros::AsyncSpinner spinner(1);
    spinner.start();

    move_group_interface::MoveGroup group("whole_body");
    group.setRandomTarget();

    group.move();
    ros::waitForShutdown();

}
